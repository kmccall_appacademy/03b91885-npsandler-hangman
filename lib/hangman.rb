class Hangman
  attr_reader :guesser, :referee, :board

  def initialize(players = {})
    @guesser = players[:guesser]
    @referee = players[:referee]
  end

  def setup
    secret_word_length = @referee.pick_secret_word
    @guesser.register_secret_length(secret_word_length)
    @board = "_" * secret_word_length
  end

  def take_turn
    letter = @guesser.guess
    indicies = @referee.check_guess(letter)
    update_board(indicies, letter)
    @guesser.handle_response
  end

  def update_board(indicies, letter)
    indicies.each do |index|
      @board[index] = letter
    end

    @board
  end

  def display_board
    puts @board
  end

  def play
    setup
    until @board.include?("_") == false
      take_turn
      display_board
    end
  end
end

class HumanPlayer
  def initialize(name)
    @name = name
  end

  def register_secret_length(length)
    puts "The length of the secret word is #{length}."
  end

  def guess(board)
    puts "Please guess a letter."
    gets.chomp
  end

  def handle_response
  end

end

class ComputerPlayer
  def initialize(dictionary)
    @dictionary = dictionary
  end

  def pick_secret_word
    @secret_word = @dictionary.sample
    @secret_word.length
  end

  def check_guess(letter)
    @secret_word.include?(letter)
    indices = []

    i = 0
    while i < @secret_word.length
      if @secret_word[i] == letter
        indices << i
      end
      i += 1
    end

    indices
  end

  def register_secret_length(length)
    words = []
    @dictionary.each do |word|
      if word.length == length
        words << word
      end
    end

    @candidate_words = words
  end

  def guess(board)
    letters_hash = Hash.new(0)

    @candidate_words.each do |word|
      word.each_char do |letter|
        letters_hash[letter] += 1
      end
    end

    greatest = "a"
    letters_hash.keys.each do |letter|
      if letters_hash[letter] > letters_hash[greatest]
        greatest = letter unless board.include?(letter)
      end
    end

    greatest
  end

  def handle_response(letter, indices)
    words = []

    if indices == []
      @candidate_words.each do |word|
        if !word.include?(letter)
          words << word
        end
      end

      @candidate_words = words

    else
      @candidate_words.each do |word|
        letter_counter = 0
        word.each_char do |char|
          if char == letter
            letter_counter += 1
          end
        end

        if word[indices[0]] == letter
          words << word unless letter_counter > indices.length
        end
      end
      @candidate_words = words
    end
  end

  def candidate_words
    @candidate_words
  end
end

if __FILE__ == $PROGRAM_NAME
  Hangman.new.play
end
